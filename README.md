# Anagram3000

For the build, please navigate to the Release folder.

##Example usages in terminal
Windows build (\bin\Release\netcoreapp2.2\win10-x64)
```
./Anagram3000.exe C:/Users/someuser/Desktop/lemmad.txt "some word"
```

Linux build (\bin\Release\netcoreapp2.2\ubuntu.16.04-x64)
```
./Anagram3000 C:/Users/someuser/Desktop/lemmad.txt "some word"
```

##Building application
Using windows runtime: 
```
dotnet build --configuration Release --runtime win10-x64
```

Using ubuntu runtime:
```
dotnet build --configuration Release --runtime ubuntu.16.04-x64
```

##.NET Core 2.2 Runtime
If you experience runtime related errors. Please make sure that required runtimes are installed to your machine.

https://dotnet.microsoft.com/download/dotnet-core/2.2


