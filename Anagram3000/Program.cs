﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Anagram3000
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();

            try
            {
                var fullPathToDictionaryFile = args[0];
                var wordToFindAnagramsFor = args[1].ToLower();

                var foundAnagrams = new List<string>();
                var dict = new Dictionary<char, int>();

                foreach (var character in wordToFindAnagramsFor)
                {
                    if (dict.ContainsKey(character)) continue;
                    dict.Add(character, wordToFindAnagramsFor.Count(x => x.Equals(character)));
                }

                using (var sr = File.OpenText(fullPathToDictionaryFile))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        if (!line.Length.Equals(wordToFindAnagramsFor.Length)) continue;
                        var hasFound = true;

                        foreach (var key in dict.Keys)
                        {
                            if (line.ToLower().Count(x => x.Equals(key)).Equals(dict[key])) continue;
                            hasFound = false;
                            break;
                        }
                        if (hasFound)
                            foundAnagrams.Add(line);
                    }
                }

                stopWatch.Stop();
                Console.WriteLine(foundAnagrams.Count >= 1
                    ? $"{ElapsedMicroSeconds(stopWatch)},{string.Join(',', foundAnagrams)}"
                    : $"Passed time in microseconds: {ElapsedMicroSeconds(stopWatch)}, Didn't find any anagrams for {string.Join("", wordToFindAnagramsFor)}");

                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Console.ReadLine();
            }
        }

        private static long ElapsedMicroSeconds(Stopwatch watch)
        {
            return (watch.ElapsedTicks * 1000000 / Stopwatch.Frequency); 
        }
    }
}
